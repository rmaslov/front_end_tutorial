define([
    'jquery',
    'underscore',
    'backbone',
    'contact/ContactsRouter'
], function ($, _, Backbone, ContactsRouter) {
    var contactsRouter = new ContactsRouter();


    return {
        inst:contactsRouter
    };
});





