define([
    'jquery',
    'underscore',
    'backbone',
    'contact/ContactModel'
], function ($, _, Backbone,ContactModel) {
    var Directory = Backbone.Collection.extend({
        model: ContactModel
    });

    return Directory;
});