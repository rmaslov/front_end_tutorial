define([
    'jquery',
    'underscore',
    'backbone',
    'contact/DirectoryView'
], function ($, _, Backbone, DirectoryView) {
    var ContactsRouter = Backbone.Router.extend({
        routes: {
            "filter/:type": "urlFilter"   ,
            "":"index"
        },

        urlFilter: function (type) {
            var directory = new DirectoryView();
            directory.filterType = type;
            directory.trigger("change:filterType");
        },
        index: function () {
            var directory = new DirectoryView();
            directory.filterType = "all";
            directory.trigger("change:filterType");
        }
    });

    return ContactsRouter;
});
