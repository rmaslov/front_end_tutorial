define([
    'jquery',
    'underscore',
    'backbone'
], function ($, _, Backbone) {
    var Contact = Backbone.Model.extend({
        defaults: {
            photo: "img/placeholder.png",
            name: "",
            address: "",
            tel: "",
            email: "",
            type: ""
        }
    });

    return Contact;
});
