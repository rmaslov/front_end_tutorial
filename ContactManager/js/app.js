define([
    'jquery',
    'underscore',
    'backbone',
    'contact/ContactRouterInstance'
], function($, _, Backbone, ContactRouterInstance){
    var initialize = function(){
        //start history service
        var contactsRouter = ContactRouterInstance;
        Backbone.history.start();
        // Pass in our Router module and call it's initialize function
    }

    return {
        initialize: initialize
    };
});





